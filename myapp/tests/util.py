# -*- coding: utf-8 -*-
__author__ = 'daeheehan'

import urllib2
import json

def post(_url, _payload):
    req = urllib2.Request(_url)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    data = json.dumps(_payload, encoding='utf-8')
    fp = urllib2.urlopen(req, data)
    response = fp.read()
    res = json.loads(response)
    return res

def split_kor_eng(line):
    n = 0
    for ch in line:
        if ch.isalpha():
            break
        n += 1
    return line[:n], line[n:]

def is_number(txt):
    for ch in txt:
        if not (ch.isdigit() or ch=='.'):
            return False
    return True


def split_kor_and_eng(flds):
    n = 0
    if flds[0][0].isalpha():
        for i in range(len(flds)):
            if not flds[i][0].isalpha():
                if i+1 < len(flds) and flds[i+1] in ['.', '?', '!']:
                    n = i+1
                else:
                    n = i
                break
        eng = ' '.join(flds[:n])
        kor = ' '.join(flds[n:])

    else:
        for i in range(len(flds)):
            if flds[i][0].isalpha():
                n = i
                break
        kor = ' '.join(flds[:n])
        eng = ' '.join(flds[n:])

    return eng, kor


def proc_line(line):
    flds = line.strip().split()
    if is_number(flds[0]):
        flds.pop(0)

    eng, kor = split_kor_and_eng(flds)
    print eng
    print kor


