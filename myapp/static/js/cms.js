$(document).ready(function() {
    $('#write-body').markdown({
        language: 'kr',
		hiddenButtons:'cmdPreview',
		footer:'<div id="body-preview" class="well" style="display:none;"></div>',
		onChange:function(e){
			var content = e.parseContent();
		    var content_length = (content.match(/\n/g)||[]).length + content.length;

			if (content == '') {
				$('#body-preview').hide()
			} else {
				$('#body-preview').show().html(content)
			}
		}
	});
});

function publishContent(elem, content_code) {
    $.ajax({
        url: '/api/1/content/' + content_code + '/publish',
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if(data && data.ok) {
                console.log(elem);
                //$(elem).attr('disabled', true);
                $(elem).text('ON');
                $(elem).removeClass('btn-default');
                $(elem).addClass('btn-success');
            }
        }
    });
}

function unpublishContent(elem, content_code) {
    $.ajax({
        url: '/api/1/content/' + content_code + '/unpublish',
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if(data && data.ok) {
                console.log(elem);
                //$(elem).attr('disabled', true);
                $(elem).text('OFF');
                $(elem).removeClass('btn-success');
                $(elem).addClass('btn-default');
            }
        }
    });
}