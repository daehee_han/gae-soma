# -*- coding: utf-8 -*-

import os
import sys
from google.appengine.api.app_identity import get_application_id

if 'libs' not in sys.path:
    sys.path[0:0] = ['libs']

APP_NAME = get_application_id()
APP_ENV = "development"
if APP_NAME.find('prd') != -1:
    APP_ENV = "production"

LOCAL_DEV = os.environ['SERVER_SOFTWARE'].startswith('Dev')

