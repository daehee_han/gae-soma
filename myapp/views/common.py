__author__ = 'daeheehan'

from google.appengine.ext import blobstore, ndb
from google.appengine.ext.webapp import blobstore_handlers
from webapp2 import uri_for

from . import ViewHandler, is_none_or_empty
from ..models import Content, Tag
import util

def content_search(self):
    query = self.request.get('query', '')
    query = query.encode('utf8').strip()

    # tag_key = Tag.query(Tag.tags == query).get().key

    contents = Content.query(Content.tags == query).fetch()
    return contents
