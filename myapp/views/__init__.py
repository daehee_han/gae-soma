# -*- coding: utf-8 -*-

import webapp2
from webapp2 import uri_for
from webapp2_extras import sessions
from ..config.jinja import JINJA_ENVIRONMENT
from ..models import Account

class ViewHandler(webapp2.RequestHandler):
    def dispatch(self):

        self.tmpltargs = {}
        self.add_targ('url_for', uri_for)

        self.session_store = sessions.get_store(request=self.request)
        self.add_targ('user', self.user)
        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            self.session_store.save_sessions(self.response)

    @webapp2.cached_property
    def session(self):
        """Returns a session using the default cookie key"""
        return self.session_store.get_session()

    @webapp2.cached_property
    def user(self):
        user_id = self.session.get('user_id')
        if user_id:
            return Account.get_by_id(user_id)

    def add_targ(self, k, v):
        ''' add template argument '''
        self.tmpltargs[k] = v

    def append_targs(self, d):
        self.tmpltargs =  dict(self.tmpltargs.items() + d.items())

    def get_rendered(self, filename, tmpltargs=None):

        template = JINJA_ENVIRONMENT.get_template(filename)
        if not tmpltargs:
            tmpltargs = self.tmpltargs
        rendered = template.render(tmpltargs)
        return rendered

    def response_html(self, filename, tmpltargs=None):
        html = self.get_rendered(filename, tmpltargs)
        self.response.write(html)

    def response_error(self, errmsg):
        return self.response.write(errmsg)

    def redirect_to_referer(self):
        return self.redirect(self.request.referer)


def is_none_or_empty(string):
    if string is None or string == '':
        return True
    return False


