# -*- coding: utf-8 -*-

from google.appengine.ext import blobstore, ndb
from google.appengine.ext.webapp import blobstore_handlers
from webapp2 import uri_for


from . import ViewHandler, is_none_or_empty
from ..models import Content, Tag, Account
from sendemail import send_email
from libs import markdown
from .util import refine_to_iframe_url_of_youtube

import datetime
import time

import sendemail
import util

class IndexView(ViewHandler):
    def get(self):
        return self.response_html('index.html')

class SigninView(ViewHandler):
    def get(self):
        return self.response_html('signin.html')

    def post(self):
        '''
        if fail, show alert message and goto signup page again.
        if success, goto index page.
        '''
        email = self.request.get('email').strip()
        passwd = self.request.get('passwd').strip()
        account = Account.query(Account.email == email).get()
        ''' CASE: email is wrong '''
        if not account:
            self.add_targ('alert', 'The email or password is wrong !')
            return self.response_html('signin.html')

        correct = util.check_passwd(passwd, account.passwd_salt, account.passwd)
        ''' CASE: passwd is wrong '''
        if not correct:
            self.add_targ('alert', 'The email or password is wrong !!')
            return self.response_html('signin.html')



        if False and not account.email_confirmed:
            nowsec = time.time()
            confirm_sec = time.mktime(account.keygen_time.timetuple())
            difftime = nowsec - confirm_sec
            if difftime > 3600: # 1 hour
                ''' CASE: confirm key expired '''
                self.add_targ('alert', 'The confirm key has been expired. Please, check it within an hour.')
                return self.response_html('fail.html')
            else:
                ''' CASE: email is NOT yet confirmed '''
                self.add_targ('alert', 'The email in NOT yet confirmed !!')
                return self.response_html('signin.html')

        account.last_time = datetime.datetime.now()
        account.put()

        self.session['user_id'] = account.key.id()
        self.add_targ('goto', '/')
        return self.response_html('success.html')

class SignupView(ViewHandler):
    def get(self):
        return self.response_html('signup.html')

    def post(self):
        '''
        if fail, show alert message and goto signup page again.
        if success, goto index page.
        '''
        email = self.request.get('email').strip()
        passwd = self.request.get('passwd').strip()

        ''' CASE: email format is NOT valid '''
        if len(email)==0:
            alert = 'Please, enter a correct email address.'
            self.add_targ('alert', alert)
            return self.response_html('signup.html')

        account = Account.query(Account.email == email).get()
        ''' CASE: email is ALREADY registered '''
        if account:
            self.add_targ('alert', 'The email is already registed. Please, sign in.')
            return self.response_html('signin.html')

        res = sendemail.check_email(email)
        ''' CASE: email is NOT a valid mail account'''
        if not res:
            alert = 'We are sorry that the email address (%s) is not reachable.' % (email)
            self.add_targ('alert', alert)
            return self.response_html('signup.html')

        ''' CASE: send confirm email '''''
        account = Account(email=email)
        account.passwd, account.passwd_salt = util.encode_passwd(passwd)
        account.confirm_key = util.gen_confirm_key(email)
        account.keygen_time= datetime.datetime.now()
        account.put()

        sendemail.send_confirm_email(email, account.confirm_key)
        self.add_targ('alert', 'Please check your email box and spam box to get an email from us.')
        self.add_targ('goto', '/')
        return self.response_html('success.html')

class SignoutView(ViewHandler):
    def get(self):
        self.session.clear()
        return self.redirect_to('index')

class PasswdView(ViewHandler):
    def get(self):
        return self.response_html('passwd.html')

class ConfirmView(ViewHandler):
    def get(self):
        key = self.request.get('key', '')
        confirm_key = key
        account = Account.query(Account.confirm_key == confirm_key).fetch(1)
        if not account:
            self.add_targ('alert', 'The URL (confirm key) is wrong')
            return self.response_html('fail.html')

        account = account[0]
        ''' Check if the key is expired '''
        nowsec = time.time()
        confirm_sec = time.mktime(account.keygen_time.timetuple())
        difftime = nowsec - confirm_sec
        if difftime > 3600: # 1 hour
            self.add_targ('alert', 'The confirm key has been expired. Please, check it within an hour.')
            return self.response_html('fail.html')

        account.confirm_time = datetime.datetime.now()
        account.email_confirmed = True
        account.put()

        self.add_targ('alert', 'Now your account has been confirmed !')
        self.add_targ('goto', '/signin')
        return self.response_html('success.html')



class SearchView(ViewHandler):
    def get(self):
        query = self.request.get('query', '')
        # query = query.encode('utf8').strip()

        # tag_key = Tag.query(Tag.tags == query).get().key

        contents = Content.query(Content.tags == query).fetch()
        contents_dict = [c for c in contents if c.is_published]
        if len(contents_dict) == 1:
            code = contents_dict[0].get_code()
            return self.redirect_to('content', content_code=code)

        self.add_targ('contents', contents_dict)
        return self.response_html('search.html')



class ContentView(ViewHandler):
    def get(self, content_code):

        if is_none_or_empty(content_code):
            return self.redirect_to('index')

        content = Content.get_by_code(content_code)

        if not content:
            return self.redirect_to('index')

        body = markdown.markdown(content.body)
        body = body.replace('\n', '</br>')

        self.add_targ('title', content.title)
        self.add_targ('body', body)

        if content.movie_url:
            self.add_targ('embed_movie_url', refine_to_iframe_url_of_youtube(content.movie_url))
        return self.response_html('content.html')






