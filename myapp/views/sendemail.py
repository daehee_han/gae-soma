# coding: utf-8
__author__ = 'daehee, pedium'

from google.appengine.api import mail

MSG = """
내웹서버에 가입하신 것을 환영하며, 감사드립니다.
아래의 링크를 클릭하시면 계정이 활성화되며, 로그인할 수 있습니다.
"""

SENDER = 'daehee<noreply@pedium.org>'


'''
return True if good
return False if fail
'''
def check_email(email):
    return mail.is_email_valid(email)

def send_email(_towhom):
    message = mail.EmailMessage(sender=SENDER,
	                            subject="[내웹서버] Your account has been approved")
    message.to = _towhom
    message.body = """
	http://myapp.com/confirm/abcdef
	myapp Team

	"""
    if not mail.is_email_valid(message.to):
        print "ERROR NOT VALID EMAIL"
        return False
    else:
        message.send()
        return True

'''
email 문자열에 대한 hashkey 구하여 문자로 발송한다
'''
def send_confirm_email(email, confirm_key):
    body = """
    Please click the link to activate your account.

    http://myapp.com/confirm?key=%s

    Thank you !

    from myapp Team
	""" % (confirm_key)

    message = mail.EmailMessage()
    message.sender = SENDER
    message.subject = "[내웹서버] Please, confirm your account"
    message.to = email
    message.body = body
    try:
        message.send()
        return 1
    except:
        return 0